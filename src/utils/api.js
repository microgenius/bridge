import axios from 'axios';
import { BASE_URL } from '../constants/urls';

export const fetchSupportedChains = async () => {
  try {
    const response = await axios.get(`${BASE_URL}/supported-chains`);
    return response.data.supportedChains;
  } catch (error) {
    alert('Error fetching supported chains:', error);
    return [];
  }
};

export const fetchTokens = async (chainId) => {
  try {
    const response = await axios.get(`${BASE_URL}/tokens?chainId=${chainId}`);
    return response.data.data.recommendedTokens;
  } catch (error) {
    alert('Error fetching tokens:', error);
    return [];
  }
};

export const fetchQuote = async (quoteParams) => {
  try {
    const response = await axios.post(`${BASE_URL}/quotes`, quoteParams, {
      headers : {
        'Content-Type': 'application/json',
      },
    });
    return response.data.data.routes;
  } catch (error) {
    alert("Error fetching quote:", error);
    throw new Error(error.response?.data?.message || 'Failed to fetch quote');
  }
};

export const postTransaction = async (params) => {
  try {
    const response = await axios.post(`${BASE_URL}/params`, params, {
    headers : {
      'Content-Type': 'application/json',
    },
  });
  return response.data;
  } catch (error) {
    alert('Error building transaction:', error);
    throw new Error(error.response?.data?.message || 'Failed to build transaction');
  }
}