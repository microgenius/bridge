import React from 'react';
import { Form } from 'react-bootstrap';

const ChainSelector = ({ chains, selectedChain, onChange, label }) => (
  <Form.Group controlId={`${label}ChainSelector`}>
    <Form.Label>{label}</Form.Label>
    <Form.Control as="select" value={selectedChain} onChange={(e) => onChange(e.target.value)}>
      <option value="">Select a chain</option>
      {chains.map((chain) => (
        <option key={chain.chainId} value={chain.chainId}>
          {chain.name}
        </option>
      ))}
    </Form.Control>
  </Form.Group>
);

export default ChainSelector;

