import React, { useState } from 'react';
import { Card, Table, Button, Modal, Alert } from 'react-bootstrap';
import { postTransaction } from '../utils/api';

const QuoteDisplay = ({ quote, onBack, onBridge }) => {
  const route = quote[0];

  const [showModal, setShowModal] = useState(false);
  const [transactionData, setTransactionData] = useState('');

  const handleBridge = async () => {
    const params = {
      srcChainId: route.srcChainId,
      srcQuoteTokenAddress: route.srcQuoteToken?.address ?? '',
      srcQuoteTokenAmount: route.srcQuoteTokenAmount ?? '',
      dstChainId: route.dstChainId,
      dstQuoteTokenAddress: route.dstQuoteToken?.address ?? '',
      slippage: 1,
      srcSwapProvider: route.srcSwapDescription?.provider ?? '',
      dstSwapProvider: route.dstSwapDescription?.provider ?? '',
      bridgeProvider: route.bridgeDescription?.provider ?? '',
      srcBridgeTokenAddress: route.bridgeDescription?.srcBridgeTokenAddress ?? '',
      dstBridgeTokenAddress: route.bridgeDescription?.dstBridgeTokenAddress ?? ''
    };

    try {
      const response = await postTransaction(params);
      setTransactionData(response);
      onBridge();
      setShowModal(true);
    } catch (error) {
      console.error('Error submitting transaction parameters:', error);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setTransactionData(null);
    onBack();
  }

  return (
    <Card className="shadow-sm" style={{ fontFamily: 'Playpen Sans' }}>
      <Card.Body>
        <Card.Title className="mb-4">Quote Details</Card.Title>
        
        {/* For mobile viewport */}
        
        <div className="d-lg-none">
          <Card.Text>
            <strong>Estimated Gas:</strong> {route.estimatedGas} units
            <br />
            <strong>Source Chain:</strong> {route.srcChainId}
            <br />
            <strong>Destination Chain:</strong> {route.dstChainId}
            <br />
            <strong>Source Token:</strong> {route.srcQuoteToken.symbol} ({route.srcQuoteToken.address})
            <br />
            <strong>Destination Token:</strong> {route.dstQuoteToken.symbol} ({route.dstQuoteToken.address})
            <br />
            <strong>Source Token Amount:</strong> {route.srcQuoteTokenAmount} {route.srcQuoteToken.symbol}
            <br />
            <strong>Destination Token Amount:</strong> {route.dstQuoteTokenAmount} {route.dstQuoteToken.symbol}
          </Card.Text>
        </div>

        {/* For desktop viewport */}
        
        <div className="d-none d-lg-block">
          <Table bordered responsive>
            <tbody>
              <tr>
                <td><strong>Estimated Gas</strong></td>
                <td>{route.estimatedGas} units</td>
              </tr>
              <tr>
                <td><strong>Source Chain</strong></td>
                <td>{route.srcChainId}</td>
              </tr>
              <tr>
                <td><strong>Destination Chain</strong></td>
                <td>{route.dstChainId}</td>
              </tr>
              <tr>
                <td><strong>Source Token</strong></td>
                <td>{route.srcQuoteToken.symbol} ({route.srcQuoteToken.address})</td>
              </tr>
              <tr>
                <td><strong>Destination Token</strong></td>
                <td>{route.dstQuoteToken.symbol} ({route.dstQuoteToken.address})</td>
              </tr>
              <tr>
                <td><strong>Source Token Amount</strong></td>
                <td>{route.srcQuoteTokenAmount} {route.srcQuoteToken.symbol}</td>
              </tr>
              <tr>
                <td><strong>Destination Token Amount</strong></td>
                <td>{route.dstQuoteTokenAmount} {route.dstQuoteToken.symbol}</td>
              </tr>
            </tbody>
          </Table>
        </div>

        <div className="text-center mt-4">
          <Button variant="secondary" onClick={onBack} className="mx-2">
            Back
          </Button>
          <Button variant="primary" onClick={handleBridge} className="mx-2">
            Bridge
          </Button>
        </div>

        {/* Modal for displaying transaction data */}
        
        <Modal show={showModal} onHide={handleCloseModal} style={{ fontFamily: 'Playpen Sans' }}>
          <Modal.Header closeButton>
            <Modal.Title>Transaction Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {transactionData ? (
              <Table bordered responsive style={{ width: '100%', tableLayout: 'fixed', wordWrap: 'break-word' }}>
                <tbody>
                  <tr>
                    <td><strong>Source Token Address</strong></td>
                    <td>{transactionData.data.route.srcQuoteTokenAddress}</td>
                  </tr>
                  <tr>
                    <td><strong>Source Token Amount</strong></td>
                    <td>{transactionData.data.route.srcQuoteTokenAmount}</td>
                  </tr>
                  <tr>
                    <td><strong>Destination Token Address</strong></td>
                    <td>{transactionData.data.route.dstQuoteTokenAddress}</td>
                  </tr>
                  <tr>
                    <td><strong>Slippage</strong></td>
                    <td>{transactionData.data.route.slippage}</td>
                  </tr>
                  <tr>
                    <td><strong>Transferred to</strong></td>
                    <td>{transactionData.data.tx.to}</td>
                  </tr>
                  <tr>
                    <td><strong>Value</strong></td>
                    <td>{transactionData.data.tx.value}</td>
                  </tr>
                </tbody>
              </Table>
            ) : (
              <p>No transaction data available</p>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={handleCloseModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>

      </Card.Body>
    </Card>
  );
};

export default QuoteDisplay;
