import React from 'react';
import { Form } from 'react-bootstrap';

const TokenSelector = ({ tokens = [], selectedToken, onChange, label }) => (
  <Form.Group controlId={`${label}TokenSelector`}>
    <Form.Label>{label}</Form.Label>
    <Form.Control as="select" value={selectedToken ? selectedToken.address : ''} onChange={(e) => {
      const selected = tokens.find(token => token.address === e.target.value);
      onChange(selected);
    }}>
      <option value="">Select a token</option>
      {tokens.map((token) => (
        <option key={token.address} value={token.address}>
          {token.name}
        </option>
      ))}
    </Form.Control>
  </Form.Group>
);

export default TokenSelector;
