import React, { useState, useEffect } from 'react';
import { Form, Button, Container, Row, Col, Card, Spinner, Alert } from 'react-bootstrap';
import ChainSelector from './components/ChainSelector';
import TokenSelector from './components/TokenSelector';
import QuoteDisplay from './components/QuoteDisplay';
import { fetchSupportedChains, fetchTokens, fetchQuote } from './utils/api';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  const [chains, setChains] = useState([]);
  const [srcTokens, setSrcTokens] = useState([]);
  const [dstTokens, setDstTokens] = useState([]);
  const [srcChain, setSrcChain] = useState('');
  const [dstChain, setDstChain] = useState('');
  const [srcToken, setSrcToken] = useState('');
  const [dstToken, setDstToken] = useState('');
  const [amount, setAmount] = useState('');
  const [quote, setQuote] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    const loadChains = async () => {
      setLoading(true);
      const chains = await fetchSupportedChains();
      setChains(chains);
      setLoading(false);
    };

    loadChains();
  }, []);

  useEffect(() => {
    const loadSrcTokens = async () => {
      if (srcChain) {
        const tokens = await fetchTokens(srcChain);
        setSrcTokens(tokens);
      } else {
        setSrcTokens([]);
      }
    };

    loadSrcTokens();
  }, [srcChain]);

  useEffect(() => {
    const loadDstTokens = async () => {
      if (dstChain) {
        const tokens = await fetchTokens(dstChain);
        setDstTokens(tokens);
      } else {
        setDstTokens([]);
      }
    };

    loadDstTokens();
  }, [dstChain]);

  const handleGetQuote = async (e) => {
    e.preventDefault();
    setError('');

    if (!srcToken || !srcToken.decimals) {
      setError('Please select a source token with decimals information.');
      return;
    }
    const decimals = srcToken.decimals;
    const quote = await fetchQuote({
      srcChainId: srcChain,
      srcQuoteTokenAddress: srcToken.address,
      srcQuoteTokenAmount: amount,
      dstChainId: dstChain,
      dstQuoteTokenAddress: dstToken.address,
      slippage: 1,
      decimals
    });
    setQuote(quote);
  };

  const handleBack = () => {
    setQuote('');
  };

  const handleBridge = () => {
    console.log('Bridging operation...');
  };

  return (
    <Container className="mt-5" style={{ fontFamily: 'Playpen Sans' }}>
      <Card className="p-4 shadow">
        <h1 className="text-center mb-4">XY Finance Token Transfer Demo</h1>
        {loading ? (
          <div className="text-center">
            <Spinner animation="border" />
          </div>
        ) : (
          !quote ? (
            <Form onSubmit={handleGetQuote}>
              <Row className="mb-4">
                <Col md={6} sm={12} className="mb-3">
                  <ChainSelector chains={chains} selectedChain={srcChain} onChange={setSrcChain} label="Source Chain" />
                </Col>
                <Col md={6} sm={12} className="mb-3">
                  <ChainSelector chains={chains} selectedChain={dstChain} onChange={setDstChain} label="Destination Chain" />
                </Col>
              </Row>
              <Row className="mb-4">
                <Col md={6} sm={12} className="mb-3">
                  <TokenSelector tokens={srcTokens} selectedToken={srcToken} onChange={setSrcToken} label="Source Token" />
                </Col>
                <Col md={6} sm={12} className="mb-3">
                  <TokenSelector tokens={dstTokens} selectedToken={dstToken} onChange={setDstToken} label="Destination Token" />
                </Col>
              </Row>
              <Row className="mb-4">
                <Col>
                  <Form.Group controlId="amount">
                    <Form.Label>Amount</Form.Label>
                    <Form.Control
                      type="string"
                      value={amount}
                      onChange={(e) => setAmount(e.target.value)}
                      placeholder="Enter amount"
                    />
                  </Form.Group>
                </Col>
              </Row>
              <div className="text-center">
                <Button variant="primary" type="submit">
                  Get Quote
                </Button>
                <br />
                <br />
              </div>
            </Form>
          ) : (
            <QuoteDisplay quote={quote} onBack={handleBack} onBridge={handleBridge} />
          )
        )}
      </Card>
    </Container>
  );
};

export default App;
