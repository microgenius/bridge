const quotesController = require('../../src/controllers/params.controller');
const xyFinanceService = require('../../src/services/xyFinance.service');

jest.mock('../../src/services/xyFinance.service');

describe('Transaction Controller', () => {
    let consoleErrorMock;
  
    beforeAll(() => {
      consoleErrorMock = jest.spyOn(console, 'error').mockImplementation(() => {});
    });
  
    afterAll(() => {
      consoleErrorMock.mockRestore();
    });
  
    describe('getParams', () => {
      it('should return a successful transaction response', async () => {
        const req = {
          body: {
            srcChainId: '1',
            srcQuoteTokenAddress: '0x123',
            srcQuoteTokenAmount: '1000',
            dstChainId: '2',
            dstQuoteTokenAddress: '0x456',
            slippage: 1,
            receiver: '0xb6EFA1C3679f1943f8aC4Fc9463Cc492435c6C92',
            srcSwapProvider: 'providerA',
            dstSwapProvider: 'providerB',
            bridgeProvider: 'providerC',
            srcBridgeTokenAddress: '0x789',
            dstBridgeTokenAddress: '0xABC'
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };
  
        const next = jest.fn();
  
        const paramsResponse = {
          success: true,
          data: {
            transaction: 'testTransaction'
          },
        };
  
        xyFinanceService.getParams.mockResolvedValue(paramsResponse);
  
        await quotesController.getParams(req, res, next);
  
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
          success: true,
          message: 'Transaction retrieved successfully',
          data: paramsResponse,
        });
      });
  
      it('should handle failed transaction response', async () => {
        const req = {
          body: {
            srcChainId: '1',
            srcQuoteTokenAddress: '0x123',
            srcQuoteTokenAmount: '1000',
            dstChainId: '2',
            dstQuoteTokenAddress: '0x456',
            slippage: 1,
            receiver: '0xb6EFA1C3679f1943f8aC4Fc9463Cc492435c6C92',
            srcSwapProvider: 'providerA',
            dstSwapProvider: 'providerB',
            bridgeProvider: 'providerC',
            srcBridgeTokenAddress: '0x789',
            dstBridgeTokenAddress: '0xABC'
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };
  
        const next = jest.fn();
  
        const paramsResponse = {
          success: false,
          errorMsg: 'Failed to retrieve transaction',
        };
  
        xyFinanceService.getParams.mockResolvedValue(paramsResponse);
  
        await quotesController.getParams(req, res, next);
  
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
          success: false,
          message: 'Failed to retrieve transaction',
          error: paramsResponse.errorMsg,
        });
      });
  
      it('should handle errors thrown during getParams', async () => {
        const req = {
          body: {
            srcChainId: '1',
            srcQuoteTokenAddress: '0x123',
            srcQuoteTokenAmount: '1000',
            dstChainId: '2',
            dstQuoteTokenAddress: '0x456',
            slippage: 1,
            receiver: '0xb6EFA1C3679f1943f8aC4Fc9463Cc492435c6C92',
            srcSwapProvider: 'providerA',
            dstSwapProvider: 'providerB',
            bridgeProvider: 'providerC',
            srcBridgeTokenAddress: '0x789',
            dstBridgeTokenAddress: '0xABC'
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };
  
        const next = jest.fn();
  
        const error = new Error('Some error');
  
        xyFinanceService.getParams.mockRejectedValue(error);
  
        await quotesController.getParams(req, res, next);
  
        expect(next).toHaveBeenCalledWith(error);
      });
    });
  });