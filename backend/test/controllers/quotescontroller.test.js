const quotesController = require('../../src/controllers/quotes.controller');
const xyFinanceService = require('../../src/services/xyFinance.service');

jest.mock('../../src/services/xyFinance.service');

describe('Quotes Controller', () => {
    let consoleErrorMock;
  
    beforeAll(() => {
      consoleErrorMock = jest.spyOn(console, 'error').mockImplementation(() => {});
    });
  
    afterAll(() => {
      consoleErrorMock.mockRestore();
    });
  
    describe('getQuote', () => {
      it('should return a successful quote response', async () => {
        const req = {
          body: {
            srcChainId: '1',
            srcQuoteTokenAddress: '0x123',
            srcQuoteTokenAmount: '1000',
            dstChainId: '2',
            dstQuoteTokenAddress: '0x456',
            slippage: 1,
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };
  
        const next = jest.fn();
  
        const quoteResponse = {
          success: true,
          data: {
            routes: [{ path: 'testPath' }],
          },
        };
  
        xyFinanceService.getQuote.mockResolvedValue(quoteResponse);
  
        await quotesController.getQuote(req, res, next);
  
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
          success: true,
          message: 'Quote retrieved successfully',
          data: quoteResponse,
        });
      });
  
      it('should handle failed quote response', async () => {
        const req = {
          body: {
            srcChainId: '1',
            srcQuoteTokenAddress: '0x123',
            srcQuoteTokenAmount: '1000',
            dstChainId: '2',
            dstQuoteTokenAddress: '0x456',
            slippage: 1,
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };
  
        const next = jest.fn();
  
        const quoteResponse = {
          success: false,
          errorMsg: 'Failed to retrieve quote',
        };
  
        xyFinanceService.getQuote.mockResolvedValue(quoteResponse);
  
        await quotesController.getQuote(req, res, next);
  
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
          success: false,
          message: 'Failed to retrieve quote',
          error: quoteResponse.errorMsg,
        });
      });
  
      it('should handle errors thrown during getQuote', async () => {
        const req = {
          body: {
            srcChainId: '1',
            srcQuoteTokenAddress: '0x123',
            srcQuoteTokenAmount: '1000',
            dstChainId: '2',
            dstQuoteTokenAddress: '0x456',
            slippage: 1,
          },
        };
  
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };
  
        const next = jest.fn();
  
        const error = new Error('Some error');
  
        xyFinanceService.getQuote.mockRejectedValue(error);
  
        await quotesController.getQuote(req, res, next);
  
        expect(next).toHaveBeenCalledWith(error);
      });
    });
  });
