const xyFinanceService = require('../services/xyFinance.service');

async function getTokens(req, res, next) {
  try {
    const chainId = req.query.chainId;
    const tokens = await xyFinanceService.getTokens(chainId);

    res.status(200).json({
      data: tokens
    });

  } catch (err) {
    console.error('Error while fetching tokens:', err.message);
    next(err);
  }
}

module.exports = {
  getTokens
};
