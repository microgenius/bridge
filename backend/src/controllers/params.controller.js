const xyFinanceService = require('../services/xyFinance.service');

async function getParams(req, res, next) {
  try {
    const {
      srcChainId,
      srcQuoteTokenAddress,
      srcQuoteTokenAmount,
      dstChainId,
      dstQuoteTokenAddress,
      slippage,
      receiver = '0xb6EFA1C3679f1943f8aC4Fc9463Cc492435c6C92',
      srcSwapProvider,
      dstSwapProvider,
      bridgeProvider,
      srcBridgeTokenAddress,
      dstBridgeTokenAddress
    } = req.body;

    const params = await xyFinanceService.getParams({
      srcChainId,
      srcQuoteTokenAddress,
      srcQuoteTokenAmount,
      dstChainId,
      dstQuoteTokenAddress,
      slippage,
      receiver,
      srcSwapProvider,
      dstSwapProvider,
      bridgeProvider,
      srcBridgeTokenAddress,
      dstBridgeTokenAddress
    });

    if (params.success) {
      res.status(200).json({
        success: true,
        message: 'Transaction retrieved successfully',
        data: params
      });
    } else {
      res.status(400).json({
        success: false,
        message: 'Failed to retrieve transaction',
        error: params.errorMsg
      });
    }
    
  } catch (err) {
    console.error('Error while fetching transaction parameters:', err);
    next(err);
  }
}

module.exports = {
  getParams
};
