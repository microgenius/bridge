const xyFinanceService = require('../services/xyFinance.service');

async function getQuote(req, res, next) {
  try {
    const { srcChainId, srcQuoteTokenAddress, srcQuoteTokenAmount, dstChainId, dstQuoteTokenAddress, slippage, decimals } = req.body;
    
    const quoteResponse = await xyFinanceService.getQuote(
      srcChainId,
      srcQuoteTokenAddress,
      srcQuoteTokenAmount,
      dstChainId,
      dstQuoteTokenAddress,
      slippage,
      decimals
    );

    if (quoteResponse.success) {
      res.status(200).json({
        success: true,
        message: 'Quote retrieved successfully',
        data: quoteResponse
      });
    } else {
      res.status(400).json({
        success: false,
        message: 'Failed to retrieve quote',
        error: quoteResponse.errorMsg
      });
    }

  } catch (err) {
    console.error('Error while fetching quote:', err);
    next(err);
  }
}

module.exports = {
  getQuote
};