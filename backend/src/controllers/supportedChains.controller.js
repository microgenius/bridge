const xyFinanceService = require('../services/xyFinance.service');

async function getSupportedChains(req, res, next) {
  try {
    const supportedChains = await xyFinanceService.getSupportedChains();
    res.json(supportedChains);
  } catch (err) {
    console.error('Error while fetching supported chains:', err.message);
    next(err);
  }
}

module.exports = {
  getSupportedChains
};
