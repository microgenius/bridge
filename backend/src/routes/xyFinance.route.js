const express = require('express');
const router = express.Router();
const tokensController = require('../controllers/tokens.controller');
const quotesController = require('../controllers/quotes.controller');
const paramsController = require('../controllers/params.controller');
const supportedChainsController = require ('../controllers/supportedChains.controller');

/*GET supported chains */
router.get('/supported-chains', supportedChainsController.getSupportedChains);

/* GET supported tokens for a specified blockchain. */
router.get('/tokens', tokensController.getTokens);

/* POST user token and chain selection to get a quote. */
router.post('/quotes', quotesController.getQuote);

/* POST transaction parameters when a user accepts a quote. */
router.post('/params', paramsController.getParams);

module.exports = router;
