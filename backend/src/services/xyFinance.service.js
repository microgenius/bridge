const axios = require('axios');

const XY_FINANCE_API_URL = 'https://aggregator-api.xy.finance/v1';

async function getSupportedChains() {
  try {
    const response = await axios.get(`${XY_FINANCE_API_URL}/supportedChains`);
    return response.data;
  } catch (error) {
    console.error('Error while fetching supported chains:', error.message);
    throw error;
  }
}

/**
 * Fetches tokens supported for a specified blockchain from XY Finance API.
 * @param {string} blockchain Blockchain name or identifier
 * @returns {Promise<Array>} Array of tokens
 */
async function getTokens(chainId) {
  try {
    const response = await axios.get(`${XY_FINANCE_API_URL}/recommendedTokens`, {
      params: { chainId }
    });
    return response.data;
  } catch (error) {
    console.error('Error while fetching tokens:', error.message);
    throw error;
  }
}

/**
 * Retrieves a quote for swapping tokens between chains using XY Finance API.
 * @param {string} srcChainId Source chain ID
 * @param {string} srcQuoteTokenAddress Source chain quote token address
 * @param {number} srcQuoteTokenAmount Source chain quote token amount
 * @param {string} dstChainId Destination chain ID
 * @param {string} dstQuoteTokenAddress Destination chain quote token address
 * @param {number} slippage Percentage of slippage tolerance
 * @returns {Promise<Object>} Quote data
 */
async function getQuote(srcChainId, srcQuoteTokenAddress, srcQuoteTokenAmount, dstChainId, dstQuoteTokenAddress, slippage, decimals) {
  try {
    const amountInWei = (srcQuoteTokenAmount * Math.pow(10, decimals)).toString();
    const params = new URLSearchParams({
      srcChainId,
      srcQuoteTokenAddress,
      srcQuoteTokenAmount: amountInWei,
      dstChainId,
      dstQuoteTokenAddress,
      slippage
    });

    const response = await axios.get(`${XY_FINANCE_API_URL}/quote?${params.toString()}`);
    return response.data;
  } catch (error) {
    console.error('Error while fetching quote:', error.message);
    throw error;
  }
}

/**
 * Builds a transaction using XY Finance API.
 * @param {Object} params Query parameters for the buildTx endpoint
 * @returns {Promise<Object>} Transaction data
 */
async function getParams({
  receiver,
  srcChainId,
  srcQuoteTokenAddress,
  srcQuoteTokenAmount,
  dstChainId,
  dstQuoteTokenAddress,
  slippage,
  srcSwapProvider,
  dstSwapProvider,
  bridgeProvider,
  srcBridgeTokenAddress,
  dstBridgeTokenAddress
}) {
  try {
    const params = new URLSearchParams({
      receiver,
      srcChainId,
      srcQuoteTokenAddress,
      srcQuoteTokenAmount,
      dstChainId,
      dstQuoteTokenAddress,
      slippage
    });

    if (srcSwapProvider) params.append('srcSwapProvider', srcSwapProvider);
    if (dstSwapProvider) params.append('dstSwapProvider', dstSwapProvider);
    if (bridgeProvider) params.append('bridgeProvider', bridgeProvider);
    if (srcBridgeTokenAddress) params.append('srcBridgeTokenAddress', srcBridgeTokenAddress);
    if (dstBridgeTokenAddress) params.append('dstBridgeTokenAddress', dstBridgeTokenAddress);

    const response = await axios.get(`${XY_FINANCE_API_URL}/buildTx?${params.toString()}`);
    return response.data;
  } catch (error) {
    console.error('Error while fetching transaction parameters:', error.message);
    throw error;
  }
}

module.exports = {
  getSupportedChains,
  getTokens,
  getQuote,
  getParams
};
