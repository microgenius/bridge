const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const xyFinanceRoutes = require('./src/routes/xyFinance.route');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use('/api', xyFinanceRoutes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
