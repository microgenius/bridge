# Bridge Backend

Backend Requirements:
1. Technology Stack:
    - Node.js with Express for the API server.

2. API Endpoints:
    - `GET /tokens`: Fetches supported tokens for a specified blockchain from the XY Finance
    API and returns them to the frontend for user selection.
    - `POST /quotes`: Receives a user's token and chain selection, queries the XY Finance
    Quotes API for a quote, and sends the data back to the frontend.
    - `POST /params`: Triggered when a user accepts a quote (clicks on "bridge"). Calls the
    XY Finance Create Transaction API to fetch the transaction parameters and returns these to
    the frontend.

3. Structure:
    - Organized the backend code with separate directories for controllers, routes, services,
    utils, and constants to ensure modularity and maintainability.

4. Testing:
    - Included backend tests using a framework like Mocha or Jest to validate API endpoints and business logic.