# Bridge

### Frontend Details:

1. Technology Stack:
    - Used React.js for the frontend.
    - Used React-Bootstrap for the UI components (minimal use).

2. Features:
    - Dynamic display of tokens fetched from the backend.
    - Form to submit token and chain selection, and display the fetched quote.
    - Display the gas needed to execute the transaction.
    - Confirmation screen to initiate the "bridge" action and display transaction parameters.

3. Structure:
    - Organize the frontend code into components, utils, constants, and URLs to maintain a
    clean structure.

4. Deployment:
    - Repository:
        - [`bridge`](https://gitlab.com/microgenius/bridge)
    - Backend Hosting:
        - [Backend Hosting on Render](https://bridge-hipc.onrender.com/api/)
    - Frontend Hosting:
        - [Frontend Hosting on Vercel](https://bridge-bay.vercel.app/)
    - Video Demo:
        - [Click here for video demo](https://drive.google.com/file/d/1Ek6NckLKOoFij6ONJAJLJ-I3ANQSZorg/view?usp=sharing)

5. Run the app locally:
    - Install NodeJS.
    - Clone the GitLab Repo.
        - Open any terminal of choice and execute the command - `git clone https://gitlab.com/microgenius/bridge.git`
        - Start the backend server first
            - cd bridge/backend
            - npm install
            - npm start (The backend server will start at port 3000)
        
        - Start the frontend after the backend is up and running.
            - cd .. (This will bring you to the root directory bridge)            
            - npm install
            - npm run dev (The front end will start at port 5173)
            - Open any browser with http://localhost:5173/
        - Test
            - cd bridge/backend
            - npm test


### Backend Details:

[Click here to read backend details](https://gitlab.com/microgenius/bridge/-/tree/main/backend?ref_type=heads)